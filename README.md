# Photo Game App
A simple mobile application allows users to upload pictures that other users can vote up
or vote down. The current winner is the one whose pictures have been both viewed the
most and up-voted the most, and least down-voted.

## Installation
- Clone the app `git clone https://bitbucket.org/simonmuthusi/photo-game-android` 
- Build the project using your favourite android build tools (preferable with Android studio and gradle)
- Install the app for testing out.

## Screenshots of the mobile app

1. Main view
![alt text](./screenshots/main.jpg)

2. Camera Launch View
![alt text](./screenshots/camera.jpg)

3. Login view
![alt text](./screenshots/login.jpg)

4. About
![alt text](./screenshots/about.jpg)
