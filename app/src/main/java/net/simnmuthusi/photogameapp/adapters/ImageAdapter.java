package net.simnmuthusi.photogameapp.adapters;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.budiyev.android.imageloader.ImageLoader;

import net.simnmuthusi.photogameapp.R;
import net.simnmuthusi.photogameapp.models.Photos;

import java.util.List;

import androidx.annotation.NonNull;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {
    private List<Photos> photosList;
    public ImageView imagePreview;
    public TextView title,category,location,description;
    Context context;
    public  class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View v) {
            super(v);
            imagePreview = v.findViewById(R.id.imagePreview);
            title = v.findViewById(R.id.txtTitle);
            category = v.findViewById(R.id.txtCategory);
            location = v.findViewById(R.id.txtLocation);
            description = v.findViewById(R.id.txtDescription);
            context = v.getContext().getApplicationContext();
        }
    }

    public ImageAdapter(List<Photos> photosList) {
        this.photosList = photosList;
    }

    @NonNull
    @Override
    public ImageAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_custom_layout, parent, false);
        return new ViewHolder(itemView);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Photos photo = photosList.get(position);
        title.setText(photo.getTitle());
        category.setText(photo.getCategory());
        location.setText(photo.getLocation());
        description.setText(photo.getDescription());

        ImageLoader.with(context).from(photo.getPhotoUrl()).load(imagePreview);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return photosList.size();
    }
}