package net.simnmuthusi.photogameapp;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import net.simnmuthusi.photogameapp.gson.User;
import net.simnmuthusi.photogameapp.network.API;
import net.simnmuthusi.photogameapp.network.APIClient;
import net.simnmuthusi.photogameapp.persistence.SessionManager;
import net.simnmuthusi.photogameapp.persistence.SettingsManager;
import net.simnmuthusi.photogameapp.util.UtilityFunction;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    EditText edUsername, edPassword;
    Button btn_login;
    View coodinatorLayout;

    SettingsManager settings;
    SessionManager sessionManager;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        settings = new SettingsManager(this);
        sessionManager = new SessionManager(this);
        user = sessionManager.getUserDetails();

        Log.e("Login",sessionManager.isLoggedIn()+"");
        if (sessionManager.isLoggedIn()) {
            Intent i = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        }

        edUsername = (EditText) findViewById(R.id.username);
        edPassword = (EditText) findViewById(R.id.password);
        coodinatorLayout = findViewById(R.id.constraintLayout);
        btn_login = (Button) findViewById(R.id.btn_login);

        btn_login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                performLogin();
            }
        });
    }

    public void performLogin() {
        final String username = edUsername.getText().toString();
        final String password = edPassword.getText().toString();

        if(validateInput(username, password)) {
            // Proceed to login
            final AlertDialog dialog = UtilityFunction.getLoadingDialog(this, getString(R.string.performing_login));
            dialog.show();

            API apiService = APIClient.getClient(LoginActivity.this).create(API.class);
            Call<ResponseBody> call = apiService.loginUser(username, password);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    dialog.dismiss();

                    try {
                        String body = response.body().string();
                        JSONObject jsonResponse = new JSONObject(body);


                        String token;
                        if(jsonResponse.has("token"))
                            token = jsonResponse.getString("token");
                        else{
                            showError(getString(R.string.generic_error), coodinatorLayout, null, null);
                            return;
                        }

                        String firstName = "";
                        if(jsonResponse.has("first_name"))
                            firstName = jsonResponse.getString("first_name");

                        String lastName = "";
                        if(jsonResponse.has("last_name"))
                            lastName = jsonResponse.getString("last_name");


                        User user = new User(username, firstName, lastName, token, new ArrayList<String>());
                        SessionManager sessionManager = new SessionManager(LoginActivity.this);
                        sessionManager.createLoginSession(user);
                        Intent i = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(i);
                        finish();


                    } catch (Exception e) {

                    }
                }
                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dialog.dismiss();
                    showConnectionError(call, t, coodinatorLayout, new View.OnClickListener(){
                        @Override
                        public void onClick(View view) {
                            performLogin();
                        }
                    }, getString(R.string.retry));
                }
            });




        }
    }

    private boolean validateInput(String username, String password) {
        boolean isValid = true;
        if (username == null || username.equals("")) {
            edUsername.setError(getString(R.string.required));
            isValid = isValid && false;
        } else {
            edUsername.setError(null);
            isValid = isValid && true;
        }

        if (password == null || password.equals("")) {
            edPassword.setError(getString(R.string.required));
            isValid = isValid && false;
        } else {
            edPassword.setError(null);
            isValid = isValid && true;
        }
        return isValid;
    }

    public void showConnectionError(Call<ResponseBody> call, Throwable t, View coodinatorLayout, View.OnClickListener action, String actionText) {

        String error = "";

        if(t instanceof IOException) {
            error = getString(R.string.connection_timed_out);
        } else if(t instanceof IllegalStateException) {
            error = getString(R.string.conversion_error);
        } else {
            error = getString(R.string.generic_error);
        }

        Snackbar snackbar = Snackbar.make(coodinatorLayout, error, Snackbar.LENGTH_LONG);
        if(action != null && (action != null || action.equals("")))
            snackbar.setAction(actionText, action);

        snackbar.show();

    }
    public void showError(String error, View coodinatorLayout, View.OnClickListener action, String actionText) {

        Snackbar snackbar = Snackbar.make(coodinatorLayout, error, Snackbar.LENGTH_LONG);
        if(action != null)
            snackbar.setAction(actionText, action);

        snackbar.show();
    }
}
