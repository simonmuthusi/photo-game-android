package net.simnmuthusi.photogameapp.util;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;

import net.simnmuthusi.photogameapp.R;
import net.simnmuthusi.photogameapp.gson.User;
import net.simnmuthusi.photogameapp.persistence.DummyProvider;
import net.simnmuthusi.photogameapp.persistence.SessionManager;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


import dmax.dialog.SpotsDialog;

import static android.content.Context.ACCOUNT_SERVICE;

public class UtilityFunction {

    /**
     * Converts a bitmap to a base64 string
     *
     * @param bitmap
     * @return
     */
    public static String convertBitmapToBase64(Bitmap bitmap) {

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();

        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

        return encoded;
    }

    /**
     * Converts a base64 string to a bitmap
     *
     * @param encodedImage
     * @return
     */
    public static Bitmap convertbase64StringToBitmap(String encodedImage) {

        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        return decodedByte;
    }

    /**
     * Returns a progress dialog to screen
     *
     * @param context
     * @param message
     * @return
     */
    public static AlertDialog getLoadingDialog(Context context, String message) {

        AlertDialog dialog = new SpotsDialog(context, message, R.style.AppTheme);

        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }

    /**
     * Returns the logged in user
     *
     * @param context
     * @return
     */
    public static Spanned getLoggedinUser(Context context) {

        SessionManager manager = new SessionManager(context);
        User user = manager.getUserDetails();

        String loggedinas = String.format(context.getString(R.string.string_logged_in_as), user.getUsername());

        return Html.fromHtml(loggedinas);
    }

    /**
     * Displays an alert dialog
     *
     * @param context
     * @param title
     * @param message
     * @param positive
     * @param positiveText
     * @param negative
     * @param negativeText
     */
    public static void showDialog(Context context, String title, String message,
                                  DialogInterface.OnClickListener positive, CharSequence positiveText,
                                  DialogInterface.OnClickListener negative, String negativeText) {

        AlertDialog.Builder dialog = new AlertDialog.Builder(context);

        dialog.setMessage(message);
        dialog.setTitle(title);

        if (positive != null && !positiveText.equals("")) {

            dialog.setPositiveButton(positiveText, positive);

        }

        if (negative != null && negativeText != null) {

            dialog.setNegativeButton(negativeText, negative);

        }

        dialog.setCancelable(false);

        dialog.create().show();
    }

    /**
     * Converts bitmap to grayscale
     *
     * @param buffer
     * @param width
     * @param height
     * @return
     */
    public static Bitmap toGrayscale(byte[] buffer, int width, int height) {

        byte[] Bits = new byte[buffer.length * 4];
        for (int i = 0; i < buffer.length; i++) {
            Bits[i * 4] = Bits[i * 4 + 1] = Bits[i * 4 + 2] = buffer[i];
            Bits[i * 4 + 3] = -1;// 0xff
        }

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        bmpGrayscale.copyPixelsFromBuffer(ByteBuffer.wrap(Bits));
        return bmpGrayscale;
    }

    /**
     * Return the string representing the quality of the fingerprint
     *
     * @param context
     * @param nfiq
     * @return
     */


    /**
     * Returns a human readable timestamp
     * @param timestamp
     * @return
     */
    public static String formatToYesterdayOrToday(long timestamp) {

        Date dateTime = new Date(timestamp);

        Calendar calendar = Calendar.getInstance();

        calendar.setTime(dateTime);

        Calendar today = Calendar.getInstance();

        Calendar yesterday = Calendar.getInstance();

        yesterday.add(Calendar.DATE, -1);

        DateFormat timeFormatter = new SimpleDateFormat("hh:mma");

        if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {

            return "Today " + timeFormatter.format(dateTime);

        } else if (calendar.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == yesterday.get(Calendar.DAY_OF_YEAR)) {

            return "Yesterday " + timeFormatter.format(dateTime);

        } else {

            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");

            return sdf.format(dateTime);
        }
    }

    /**
     * Create a dummy account that is used in sync adapter
     * @param context
     * @return
     */
    public static Account createDummyAccount(Context context) {

        Account dummyAccount = new Account("busara_center_lab", "org.busaracenterlab.busaracenterlab");

        AccountManager accountManager = (AccountManager) context.getSystemService(ACCOUNT_SERVICE);

        accountManager.addAccountExplicitly(dummyAccount, null, null);

        ContentResolver.setSyncAutomatically(dummyAccount, DummyProvider.AUTHORITY, true);

        return dummyAccount;
    }

    /**
     * Trigger sync adapter to send data to the server.
     * @param context
     */
    public static void triggerSync(Context context) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        ContentResolver.requestSync(UtilityFunction.createDummyAccount(context), DummyProvider.AUTHORITY, bundle);
    }

    /**
     * Returns content of a private file
     * @param context
     * @param filename
     * @return
     */
    public static String getJsonFromCache(Context context, String filename) {

        try {
            byte[] buffer = new byte[1024];
            int length;
            StringBuffer fileContent = new StringBuffer("");
            FileInputStream fis = context.openFileInput(filename);

            while ((length = fis.read(buffer)) != -1) {

                fileContent.append(new String(buffer));
            }

            fis.close();

            return fileContent.toString();

        } catch (FileNotFoundException e) {
            return null;
        } catch (IOException ex) {
            return null;
        }
    }

    /**
     * Generates the name of a file dynamically
     *
     * @param context Context object
     * @return String filename
     */
    public static String getFilename(Context context) {

        SessionManager sessionManager = new SessionManager(context);
        User userDetails = sessionManager.getUserDetails();

        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH:mm:ss");
        Date date = new Date();

        String filename = userDetails.getUsername() + "_" + dateFormat.format(date) + ".csv";

        return filename;
    }

    public String convertDate(long date) {

        Calendar calendar = new GregorianCalendar();

        calendar.setTimeInMillis(date);

        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");

        formatter.setCalendar(calendar);


        String convertedDate = formatter.format(calendar.getTime());

        return convertedDate;
    }

}

