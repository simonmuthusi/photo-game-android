package net.simnmuthusi.photogameapp;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.vistrav.ask.Ask;

import net.simnmuthusi.photogameapp.fragments.AboutFragment;
import net.simnmuthusi.photogameapp.fragments.CameraFragment;
import net.simnmuthusi.photogameapp.fragments.HomeFragment;
import net.simnmuthusi.photogameapp.gson.User;
import net.simnmuthusi.photogameapp.models.Photos;
import net.simnmuthusi.photogameapp.network.API;
import net.simnmuthusi.photogameapp.network.APIClient;
import net.simnmuthusi.photogameapp.persistence.SessionManager;
import net.simnmuthusi.photogameapp.persistence.SettingsManager;
import net.simnmuthusi.photogameapp.util.UtilityFunction;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.List;

import io.hypertrack.smart_scheduler.Job;
import io.hypertrack.smart_scheduler.SmartScheduler;
import okhttp3.ResponseBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements CameraFragment.OnFragmentInteractionListener,
        HomeFragment.OnFragmentInteractionListener,
        AboutFragment.OnFragmentInteractionListener,
        SmartScheduler.JobScheduledCallback {

    private TextView mTextMessage;

    SettingsManager settings;
    SessionManager sessionManager;
    User user;

    Context context;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            FragmentTransaction ft;
            switch (item.getItemId()) {
                case R.id.navigation_dashboard:
                    HomeFragment hom = new HomeFragment();
                    ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frag_main_layout, hom);
                    ft.commit();
                    return true;
                case R.id.navigation_camera:
                    context = getApplicationContext();
                    EasyImage.openChooserWithGallery(MainActivity.this, "Take a Photo or Select", 0);
                    return true;
                case R.id.navigation_about:
                    AboutFragment about = new AboutFragment();
                    ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frag_main_layout, about);
                    ft.commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = getApplicationContext();
        sessionManager = new SessionManager(context);
        if (!sessionManager.isLoggedIn()) {
            Intent i = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(i);
            finish();
        }

        SmartScheduler jobScheduler = SmartScheduler.getInstance(this);
        Job job = createJob();
        if (jobScheduler.addJob(job)) {
            Toast.makeText(MainActivity.this, "Job successfully added!", Toast.LENGTH_SHORT).show();
        }



        // ask for permissions
        Ask.on(this)
                .id(21313123) // in case you are invoking multiple time Ask from same activity or fragment
                .forPermissions(Manifest.permission.CAMERA
                        , Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withRationales("To use camera, will need Camera permissions to take pictures",
                        "To store media locally, will need storage permissions") //optional
                .go();


        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

       startHomeFragment();
        getMedia();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
                e.printStackTrace();
            }

            @Override
            public void onImagesPicked(List<File> imageFiles, EasyImage.ImageSource source, int type) {
                onPhotosReturned(imageFiles);
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
            }
        });
    }
    private void onPhotosReturned(List<File> returnedPhotos) {
        CameraFragment cam = new CameraFragment();

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();


        Bundle args = new Bundle();
        args.putString("takenPhoto", returnedPhotos.get(0).toString());
        cam.setArguments(args);

        ft.replace(R.id.frag_main_layout, cam);
        ft.commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void startHomeFragment()
    {
        HomeFragment hom = new HomeFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frag_main_layout, hom);
        ft.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_logout){
           // logout user
            sessionManager.logoutUser();
        }
        return super.onOptionsItemSelected(item);
    }

    public void getMedia(){
        Toast.makeText(this.getApplicationContext(), "Loading photos", Toast.LENGTH_LONG);
        API apiService = APIClient.getClient(MainActivity.this).create(API.class);
        Call<ResponseBody> call = apiService.getPhotos();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    String body = response.body().string();

                    JSONArray jsonResponse = new JSONArray(body);

                    // delete if we have more records
                    if(jsonResponse.length()>0){
                        Photos.deleteAll(Photos.class);
                    }

                    for(int i=0; i<jsonResponse.length();i++){

                        JSONObject photoItem = jsonResponse.getJSONObject(i);
                        Photos ph = new Photos();
                        ph.setTitle(photoItem.getString("title"));
                        ph.setDescription(photoItem.getString("description"));
                        ph.setPhotoUrl(photoItem.getString("photo_file"));
                        ph.setCategory(photoItem.getString("category"));
                        ph.setLocation(photoItem.getString("location"));
                        ph.setTimeTaken(photoItem.getString("time_taken"));
                        ph.setPhotoId(photoItem.getInt("id"));
                        ph.save();

                    }
                }catch(Exception e){
                    e.printStackTrace();
                }finally {
                   // do the final call here
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }
    private Job createJob() {
        int jobType = Job.Type.JOB_TYPE_ALARM;// Job.Type.JOB_TYPE_PERIODIC_TASK;
        int networkType = Job.NetworkType.NETWORK_TYPE_ANY;
        boolean requiresCharging = false;
        boolean isPeriodic = true;
        int intervalInMillis = 1000;
        int JOB_ID = 1;
        String JOB_PERIODIC_TASK_TAG = "net.simnmuthusi.photogameapp";


        Job.Builder builder = new Job.Builder(JOB_ID, this, jobType, JOB_PERIODIC_TASK_TAG)
                .setRequiredNetworkType(networkType)
                .setRequiresCharging(requiresCharging)
                .setIntervalMillis(intervalInMillis);

        if (isPeriodic) {
            builder.setPeriodic(intervalInMillis);
        }

        return builder.build();
    }

    @Override
    public void onJobScheduled(Context context, final Job job) {
        if (job != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MainActivity.this, "Job: " + job.getJobId() + " scheduled!", Toast.LENGTH_SHORT).show();
                }
            });
            Log.e("TAG", "Job: " + job.getJobId() + " scheduled!");

        }

    }
}
