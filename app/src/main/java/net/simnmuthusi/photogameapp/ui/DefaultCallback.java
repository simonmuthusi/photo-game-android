package net.simnmuthusi.photogameapp.ui;

import java.io.File;
import java.util.List;

import pl.aprilapps.easyphotopicker.EasyImage;

public abstract class DefaultCallback implements EasyImage.Callbacks {

    @Override
    public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
    }

    @Override
    public void onCanceled(EasyImage.ImageSource source, int type) {
    }

//    @Override
//    public void onImagesPicked(List<File> imageFiles, EasyImage.ImageSource source, int type) {
////        onPhotosReturned(imageFiles);
//    }
}