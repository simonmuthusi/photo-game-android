package net.simnmuthusi.photogameapp.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import net.simnmuthusi.photogameapp.R;
import net.simnmuthusi.photogameapp.adapters.ImageAdapter;
import net.simnmuthusi.photogameapp.models.Photos;

import java.util.List;

public class StartActivity extends AppCompatActivity {

    RecyclerView mRecyclerView;
    private List<Photos> photosList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        mRecyclerView = findViewById(R.id.recyclerview);
        GridLayoutManager mGridLayoutManager = new GridLayoutManager(StartActivity.this, 2);
        mRecyclerView.setLayoutManager(mGridLayoutManager);



        ImageAdapter myAdapter = new ImageAdapter(photosList);
        mRecyclerView.setAdapter(myAdapter);
    }
}
