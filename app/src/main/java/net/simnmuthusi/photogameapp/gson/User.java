package net.simnmuthusi.photogameapp.gson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class User {

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("first_name")
    @Expose
    private String firstName;

    @SerializedName("last_name")
    @Expose
    private String lastName;

    @SerializedName("token")
    @Expose
    private String token;

    @SerializedName("groups")
    @Expose
    private List<String> groups = null;

    public User(String firstName, String lastName, String token, List<String> groups) {
        this.username = "";
        this.firstName = firstName;
        this.lastName = lastName;
        this.token = token;
        this.groups = groups;
    }

    public User(String username, String firstName, String lastName, String token, List<String> groups) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.token = token;
        this.groups = groups;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<String> getGroups() {
        return groups;
    }

    public void setGroups(List<String> groups) {
        this.groups = groups;
    }

}

