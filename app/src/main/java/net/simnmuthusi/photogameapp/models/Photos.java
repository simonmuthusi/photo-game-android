package net.simnmuthusi.photogameapp.models;

import com.orm.SugarRecord;

public class Photos extends SugarRecord {
    private String title;
    private String imageTaken;
    private String description;
    private String category;
    private String owner;
    private String location;
    private String photoUrl;
    private int photoId;

    public Photos(){
    }

    public Photos(String title, String imageTaken, String description, String category, String owner, String location, String photoUrl, int photoId, String timeTaken) {
        this.title = title;
        this.imageTaken = imageTaken;
        this.description = description;
        this.category = category;
        this.owner = owner;
        this.location = location;
        this.photoUrl = photoUrl;
        this.photoId = photoId;
        this.timeTaken = timeTaken;
    }

    public int getPhotoId() {
        return photoId;
    }

    public void setPhotoId(int photoId) {
        this.photoId = photoId;
    }

    public String getTitle() {
        return title;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageTaken() {
        return imageTaken;
    }

    public void setImageTaken(String imageTaken) {
        this.imageTaken = imageTaken;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(String timeTaken) {
        this.timeTaken = timeTaken;
    }

    private String timeTaken;

}
