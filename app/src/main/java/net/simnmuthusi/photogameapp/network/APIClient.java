package net.simnmuthusi.photogameapp.network;

import android.content.Context;

import net.simnmuthusi.photogameapp.persistence.SettingsManager;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * LabApiClient
 * A singleton class that defines a Retrofit http client to access the lab api
 */
public class APIClient {

    private static Retrofit retrofit = null;

    public static Retrofit getClient(Context context) {

        SettingsManager settings = new SettingsManager(context);

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
//        httpClient.connectTimeout(2, TimeUnit.SECONDS);
        httpClient.addInterceptor(logging);

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(settings.getSettingApiEndpoint())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();
        }
        return retrofit;
    }
}