package net.simnmuthusi.photogameapp.network;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface API {

    @FormUrlEncoded
    @POST("/api/v1/users/login/")
    Call<ResponseBody> loginUser(@Field("username") String username,
                                 @Field("password") String password);

    // change getting token to SessionManager later
    @Headers("Authorization: Token bfbc55ae0b771a557945259ab1b171bdf5b9f87c")
    @GET("/api/v1/photos/")
    Call<ResponseBody> getPhotos();
}
