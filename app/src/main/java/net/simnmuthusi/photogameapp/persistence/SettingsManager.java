package net.simnmuthusi.photogameapp.persistence;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import net.simnmuthusi.photogameapp.R;


public class SettingsManager {


    SharedPreferences pref;

    SharedPreferences.Editor editor;

    Context context;

    public SettingsManager(Context context) {

        this.context = context;
        this.pref = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public boolean getSettingConfigDataOffline() {
        return this.pref.getBoolean("config_save_offline", true);
    }

    public String getSettingConfigImageQuality() {
        return this.pref.getString("config_image_quality", "medium");
    }

    public int getImageWidth() {

        String quality = this.getSettingConfigImageQuality();

        switch (quality) {

            case "low":
                return this.context.getResources().getInteger(R.integer.config_low_width);

            case "medium":
                return this.context.getResources().getInteger(R.integer.config_medium_width);

            case "high":
                return this.context.getResources().getInteger(R.integer.config_high_width);

            default:
                return 0;
        }

    }

    public int getImageHeight() {

        String quality = this.getSettingConfigImageQuality();

        switch (quality) {

            case "low":
                return this.context.getResources().getInteger(R.integer.config_low_height);

            case "medium":
                return this.context.getResources().getInteger(R.integer.config_medium_height);

            case "high":
                return this.context.getResources().getInteger(R.integer.config_high_height);

            default:
                return 0;
        }

    }

    public String getSettingApiEndpoint() {
        return this.pref.getString("config_api_endpoint", this.context.getString(R.string.production_endpoint));
    }

    public boolean getSettingAutosync() {
        return this.pref.getBoolean("config_autosync", true);
    }

    public boolean getLocationSetting() {
        return this.pref.getBoolean("config_location", true);
    }
}
