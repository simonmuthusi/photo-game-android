package net.simnmuthusi.photogameapp.persistence;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;

import net.simnmuthusi.photogameapp.LoginActivity;
import net.simnmuthusi.photogameapp.gson.User;

import java.util.Arrays;

public class SessionManager {

    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "8eNqmdRPCjqAxWCv8tk23j8PQnTxRcgNVCsddQpqzqKqAK";

    public static final String IS_LOGGED = "IsLoggedIn";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_FIRSTNAME = "firstName";
    public static final String KEY_LASTNAME = "lastName";
    public static final String KEY_TOKEN = "token";
    public static final String KEY_GROUPS = "groups";

    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     */
    public void createLoginSession(User user) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGGED, true);

        // Storing name in pref
        editor.putString(KEY_USERNAME, user.getUsername());
        editor.putString(KEY_FIRSTNAME, user.getFirstName());
        editor.putString(KEY_LASTNAME, user.getLastName());
        editor.putString(KEY_TOKEN, user.getToken());

        String groups[] = user.getGroups().toArray(new String[0]);
        editor.putString(KEY_GROUPS, TextUtils.join(",", groups));

        // commit changes
        editor.commit();
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     */
    public void checkLogin() {
        // Check login status
        if (!this.isLoggedIn()) {

            Intent i = new Intent(_context, LoginActivity.class);

            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            _context.startActivity(i);
        }

    }


    /**
     * Get stored session data
     */
    public User getUserDetails() {

        String username = pref.getString(KEY_USERNAME, "");
        String firstName = pref.getString(KEY_FIRSTNAME, "");
        String lastName = pref.getString(KEY_LASTNAME, "");
        String token = pref.getString(KEY_TOKEN, "");
        String groups = pref.getString(KEY_GROUPS, "");

        User user = new User(username, firstName, lastName, token, Arrays.asList(groups.split(",")));

        return user;
    }

    /**
     * Update user details
     */
    public void editUserValue(String key, String value) {

        editor.putString(key, value);

        editor.commit();
    }

    /**
     * Clear session details
     */
    public void logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        Intent i = new Intent(_context, LoginActivity.class);

        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        _context.startActivity(i);
    }

    /**
     * Quick check for login
     **/
    // Get Login State
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGGED, false);
    }
    public String getKeyToken(){
        return pref.getString(KEY_TOKEN, "");
    }
}