package net.simnmuthusi.photogameapp.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.vistrav.ask.Ask;

import net.simnmuthusi.photogameapp.MainActivity;
import net.simnmuthusi.photogameapp.R;
import net.simnmuthusi.photogameapp.models.Photos;
import net.simnmuthusi.photogameapp.util.UtilityFunction;

import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CameraFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CameraFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CameraFragment extends Fragment {

    private String takenPhoto = null;
    private ImageView imgTaken;
    private TextView title, category, description, location;
    private Button btn_save;

    private OnFragmentInteractionListener mListener;

    public CameraFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment CameraFragment.
     */
    public static CameraFragment newInstance(String takenPhoto) {
        CameraFragment fragment = new CameraFragment();
        Bundle args = new Bundle();
        args.putString("takenPhoto", takenPhoto);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            takenPhoto = getArguments().getString("takenPhoto");
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Ask.on(this)
                .id(21313123) // in case you are invoking multiple time Ask from same activity or fragment
                .forPermissions(Manifest.permission.CAMERA
                        , Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withRationales("To use camera, will need Camera permissions to take pictures",
                        "To store media locally, will need storage permissions") //optional
                .go();
        // Inflate the layout for this fragment
        View view  =  inflater.inflate(R.layout.fragment_camera, container, false);

        imgTaken = view.findViewById(R.id.takenPhoto);
        title = view.findViewById(R.id.photoTitle);
        category = view.findViewById(R.id.photoCategory);
        location = view.findViewById(R.id.photoLocation);
        description = view.findViewById(R.id.photoDescription);
        btn_save = view.findViewById(R.id.btn_photo_save);

        if(takenPhoto == null){
            startHomeFragment(); return view;
        }
        try {
            File imgFile = new File(takenPhoto);

            if (imgFile.exists()) {
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                imgTaken.setImageBitmap(myBitmap);

            }
        }catch(Exception e){
            e.printStackTrace();
        }

//        declare click event for kthe button
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String p_image = takenPhoto;
                String p_title = title.getText().toString();
                String p_category = category.getText().toString();
                String p_description = description.getText().toString();
                String p_owner = "1";
                String p_location = location.getText().toString();
                long now = System.currentTimeMillis();
                UtilityFunction utl = new UtilityFunction();
                String p_time_taken = utl.convertDate(now);

                Photos photo = new Photos();
                photo.setTitle(p_title);
                photo.setImageTaken(p_image);
                photo.setCategory(p_category);
                photo.setDescription(p_description);
                photo.setOwner(p_owner);
                photo.setLocation(p_location);
                photo.setTimeTaken(p_time_taken);

                photo.save();

                List<Photos> photos = Photos.listAll(Photos.class);
                Toast.makeText(getContext(), "Photo saved successfully", Toast.LENGTH_LONG).show();

                startHomeFragment();
            }
        });


        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    private void startHomeFragment()
    {
        //                Start home fragment
        HomeFragment hom = new HomeFragment();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frag_main_layout, hom);
        ft.commit();

    }
}
